
#!/bin/sh
cd /home/deployer/streamit
git checkout main
#git reset --hard origin/main
git pull
cd client && sudo npm install
sudo npm run build
sudo kill -9 $(sudo lsof -t -i:8080)
sleep 2
cd /home/deployer/streamit/server
sudo nohup npm start > myprogram.out 2>&1 &
sleep 2
cat myprogram.out
