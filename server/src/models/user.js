const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;
const userSchema = Schema(
  {
    name: {
      type: String,
      required: [true, "Name is required!"],
      trim: true,
      validate(value) {
        if (!value.match(/^[A-Za-z\s]+$/)){
          throw new Error('Only Alphabets Allowed')
        }
      }
    },
    username: {
      type: String,
      unique: [true,"Username {VALUE} already exists in the database!"],
      required: [true,"Username cannot be empty!"],
      trim: true,
      lowercase: true,
    },
    email: {
      type: String,
      unique: [true, "Email {VALUE} already exists in database!"],
      required: [true,"Email is required!"],
      trim: true,
      lowercase: true,
      //Change start here
      validate: {
        validator: function (v) {
          return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(v);
        },
        message: '{VALUE} is not a valid email!'
      }
      //Chnage end here
      /*
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('email is invalid');
        }
      },
      */
    },
    password: {
      type: String,
      required: [true, "Password cannot be empty!"],
      trim: true,
      //minlength: 8,
      validate(value) {
        if (value.toLowerCase().includes('password')) {
          throw new Error('Password should not contain word: password');
        }
        if (value.length === '' ||  value.length < 8) {
          throw new Error('Password is too short');
          //erro_code = 2;
        }
        if (value.length >= 8 && value.length < 40) {
          if (!value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,40}$/)) {
            throw new Error('Password should contain atleast one special character');
           }
        }

        //changes start here
        
        //if (!value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/)) {
        //  throw new Error('Password should contain atleast one special character');
        //}
        /*
        
        if (value.length === '' ||  value.length < 8) {
          throw new Error('Password is too short');
          //erro_code = 2;
        }
        //if (value.length === '' ||  value.length < 8) {
        //  throw new Error('put more character');
        //  erro_code = 2;
        //}
        if (!value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/)) {
          throw new Error('Password must contain atleast one: Upper Case, \n Lower case\n Number and special character');
        }
*/
        //Changes end here

      },
    },
    role: {
      type: String,
      default: 'guest',
      enum: ['guest', 'admin', 'superadmin'],
    },

    facebook: String,
    google: String,

    phone: {
      type: String,
      unique: [true, "Phone already exists in database!"],
      required: [true, "Phone cannot be empty"],
      trim: true,
      validate(value) {
        if (!validator.isMobilePhone(value)) {
          throw new Error('Phone is invalid');
        }
      },
    },
    imageurl: {
      type: String,
    },
    tokens: [
      {
        token: {
          type: String,
          required: true,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

userSchema.methods.toJSON = function() {
  const user = this;
  const userObject = user.toObject();
  if (!userObject.role === 'superadmin') {
    delete userObject.updatedAt;
    delete userObject.__v;
  }
  delete userObject.password;
  delete userObject.tokens;

  return userObject;
};

userSchema.methods.generateAuthToken = async function() {
  const user = this;
  const token = jwt.sign({ _id: user._id.toString() }, 'mySecret');
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
};

userSchema.statics.findByCredentials = async (username, password) => {
  const user = await User.findOne({ username });
  if (!user) throw new Error('Unable to login');

  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) throw new Error('Unable to login');

  return user;
};

// Hash the plain text password before save
userSchema.pre('save', async function(next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;
