import React from 'react';
import { Divider, Typography, Link } from '@material-ui/core';
import useStyles from './styles';

export default function Footer() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Divider />
      <Typography className={classes.copyright} variant="body1">
        SPW Project - 2022
      </Typography>
      <Typography variant="caption">
        Secured by Deven, Stephen, Priti and Rahul |{' '}
        <Link href="https://dopesec.co/" target="_blank" rel="noopener">
          SPW Group Project
        </Link>
      </Typography>
    </div>
  );
}
